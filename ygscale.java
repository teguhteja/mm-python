import java.util.*;
import java.sql.*;
import java.text.*;
import java.io.*;
import java.net.*;
import java.lang.*;

public class ygscale {

	public static String trimRight(String string) {
		char[] c = string.toCharArray();
	    int length = c.length;
	    while (c[length - 1] <= ' ') {
		    length--;
		}
		return string.substring(0, length);
	}

	public static BufferedReader in = null;
    Timer timer = null;
    String filename;
    String hostname;
    String columnSeparator = "";
    String columnSeparator1 = ".";
    String columnSeparator2 = "	";
	String rowSeparator = "\r\n";

    public static void main(String args[])
    {
		ygscale yg = new ygscale();
		System.out.println("Otomasi update data timbangan. Version 1.0beta");
		System.out.println("----------------------------------------------");
		yg.jadwal();
	}

    public synchronized void jadwal() {
	    timer = new Timer();
	    timer.schedule(new TransferData(),0,60000);

    }

    class TransferData extends TimerTask {
		int TransferData = 100;
		public void run() {
			if (TransferData > 0) {
				java.util.Date now = new java.util.Date();
				java.sql.Date sqlDate = new java.sql.Date(now.getTime());
				SimpleDateFormat format = new SimpleDateFormat("dd MM yyyy hh:mm:ss");
				String s = format.format(sqlDate);
				System.out.println(s);
				update1();
				update2();
				update3();
				update4();
				update5();
				TableToDat();
				hostfile();
				transfer();

			}

			else {
				System.out.println("Keluar Program%n");
				System.exit(0);
			}
		}


		void update1() {
			try	{
				Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
				Connection dbfcon = DriverManager.getConnection("jdbc:odbc:inv");
				Connection mdbcon = DriverManager.getConnection("jdbc:odbc:ygscale");
				Statement dbfstmt1 = dbfcon.createStatement();
				ResultSet dbfrs1 = dbfstmt1.executeQuery("select article,jual,plu from inv095 where rec > 0");
				PreparedStatement mdbps = mdbcon.prepareStatement("select * from Data_Barang where kode_plu = ?");
				PreparedStatement mdbup = mdbcon.prepareStatement("update Data_Barang set harga=? where kode_plu=?");
				PreparedStatement mdbin = mdbcon.prepareStatement("insert into Data_Barang(kode_pgl,nama,harga,kode_plu) values (?,?,?,?)");
				int rowCount1=0,rowCount2=0;
				while(dbfrs1.next()) {
					mdbps.setDouble(1,dbfrs1.getDouble(3));
					ResultSet mdbrs = mdbps.executeQuery();
					if(mdbrs.next()) {
						mdbup.setDouble(1,dbfrs1.getDouble(2));
						mdbup.setDouble(2,dbfrs1.getDouble(3));
						mdbup.executeUpdate();
						rowCount1++;
					} else {
						Statement mdbstmt = mdbcon.createStatement();
						ResultSet koders = mdbstmt.executeQuery("select MAX(KODE_PGL) from Data_Barang");
						while(koders.next()) {
							mdbin.setInt(1,koders.getInt(1)+1);
							mdbin.setString(2,dbfrs1.getString(1));
							mdbin.setDouble(3,dbfrs1.getDouble(2));
							mdbin.setDouble(4,dbfrs1.getDouble(3));
							mdbin.executeUpdate();
							rowCount2++;
						}
					}
				}
				dbfrs1.close();
				mdbup.close();
				dbfcon.close();
				mdbcon.close();
				System.out.println("	" + rowCount1 + " data yang di update");
				System.out.println("	" + rowCount2 + " data baru yang masuk");
				System.out.println("	Data Pre-cooked sudah diupdate ");
				System.out.println("");
			}

			catch(Exception e)
			{
				e.printStackTrace();
				System.out.println(e.getMessage());
				System.out.println("	Kode Panggil atau Kode PLU sudah ada. Data gagal di masukan!");
			}
		}

		void update2() {
			try	{
				Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
				Connection dbfcon = DriverManager.getConnection("jdbc:odbc:inv");
				Connection mdbcon = DriverManager.getConnection("jdbc:odbc:ygscale");
				Statement dbfstmt1 = dbfcon.createStatement();
				ResultSet dbfrs1 = dbfstmt1.executeQuery("select article,jual,plu from inv096 where rec > 0");
				PreparedStatement mdbps = mdbcon.prepareStatement("select * from Data_Barang where kode_plu = ?");
				PreparedStatement mdbup = mdbcon.prepareStatement("update Data_Barang set harga=? where kode_plu=?");
				PreparedStatement mdbin = mdbcon.prepareStatement("insert into Data_Barang(kode_pgl,nama,harga,kode_plu) values (?,?,?,?)");
				int rowCount1=0,rowCount2=0;
				while(dbfrs1.next()) {
					mdbps.setDouble(1,dbfrs1.getDouble(3));
					ResultSet mdbrs = mdbps.executeQuery();
					if(mdbrs.next()) {
						mdbup.setDouble(1,dbfrs1.getDouble(2));
						mdbup.setDouble(2,dbfrs1.getDouble(3));
						mdbup.executeUpdate();
						rowCount1++;
					} else {
						Statement mdbstmt = mdbcon.createStatement();
						ResultSet koders = mdbstmt.executeQuery("select MAX(KODE_PGL) from Data_Barang");
						while(koders.next()) {
							mdbin.setInt(1,koders.getInt(1)+1);
							mdbin.setString(2,dbfrs1.getString(1));
							mdbin.setDouble(3,dbfrs1.getDouble(2));
							mdbin.setDouble(4,dbfrs1.getDouble(3));
							mdbin.executeUpdate();
							rowCount2++;
						}
					}
				}
				dbfrs1.close();
				mdbup.close();
				dbfcon.close();
				mdbcon.close();
				System.out.println("	" + rowCount1 + " data yang di update");
				System.out.println("	" + rowCount2 + " data baru yang masuk");
				System.out.println("	Data Home Brand sudah diupdate ");
				System.out.println("");
			}

			catch(Exception e)
			{
				e.printStackTrace();
				System.out.println(e.getMessage());
				System.out.println("	Kode Panggil atau Kode PLU sudah ada. Data gagal di masukan!");
			}
		}

		void update3() {
			try	{
				Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
				Connection dbfcon = DriverManager.getConnection("jdbc:odbc:inv");
				Connection mdbcon = DriverManager.getConnection("jdbc:odbc:ygscale");
				Statement dbfstmt1 = dbfcon.createStatement();
				ResultSet dbfrs1 = dbfstmt1.executeQuery("select article,jual,plu from inv097 where rec > 0");
				PreparedStatement mdbps = mdbcon.prepareStatement("select * from Data_Barang where kode_plu = ?");
				PreparedStatement mdbup = mdbcon.prepareStatement("update Data_Barang set harga=? where kode_plu=?");
				PreparedStatement mdbin = mdbcon.prepareStatement("insert into Data_Barang(kode_pgl,nama,harga,kode_plu) values (?,?,?,?)");
				int rowCount1=0,rowCount2=0;
				while(dbfrs1.next()) {
					mdbps.setDouble(1,dbfrs1.getDouble(3));
					ResultSet mdbrs = mdbps.executeQuery();
					if(mdbrs.next()) {
						mdbup.setDouble(1,dbfrs1.getDouble(2));
						mdbup.setDouble(2,dbfrs1.getDouble(3));
						mdbup.executeUpdate();
						rowCount1++;
					} else {
						Statement mdbstmt = mdbcon.createStatement();
						ResultSet koders = mdbstmt.executeQuery("select MAX(KODE_PGL) from Data_Barang");
						while(koders.next()) {
							mdbin.setInt(1,koders.getInt(1)+1);
							mdbin.setString(2,dbfrs1.getString(1));
							mdbin.setDouble(3,dbfrs1.getDouble(2));
							mdbin.setDouble(4,dbfrs1.getDouble(3));
							mdbin.executeUpdate();
							rowCount2++;
						}
					}
				}
				dbfrs1.close();
				mdbup.close();
				dbfcon.close();
				mdbcon.close();
				System.out.println("	" + rowCount1 + " data yang di update");
				System.out.println("	" + rowCount2 + " data baru yang masuk");
				System.out.println("	Data Ikan dan Daging sudah diupdate ");
				System.out.println("");
			}

			catch(Exception e)
			{
				e.printStackTrace();
				System.out.println(e.getMessage());
				System.out.println("	Kode Panggil atau Kode PLU sudah ada. Data gagal di masukan!");
			}
		}


		void update4() {
			try	{
				Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
				Connection dbfcon = DriverManager.getConnection("jdbc:odbc:inv");
				Connection mdbcon = DriverManager.getConnection("jdbc:odbc:ygscale");
				Statement dbfstmt1 = dbfcon.createStatement();
				ResultSet dbfrs1 = dbfstmt1.executeQuery("select article,jual,plu from inv098 where rec > 0");
				PreparedStatement mdbps = mdbcon.prepareStatement("select * from Data_Barang where kode_plu = ?");
				PreparedStatement mdbup = mdbcon.prepareStatement("update Data_Barang set harga=? where kode_plu=?");
				PreparedStatement mdbin = mdbcon.prepareStatement("insert into Data_Barang(kode_pgl,nama,harga,kode_plu) values (?,?,?,?)");
				int rowCount1=0,rowCount2=0;
				while(dbfrs1.next()) {
					mdbps.setDouble(1,dbfrs1.getDouble(3));
					ResultSet mdbrs = mdbps.executeQuery();
					if(mdbrs.next()) {
						mdbup.setDouble(1,dbfrs1.getDouble(2));
						mdbup.setDouble(2,dbfrs1.getDouble(3));
						mdbup.executeUpdate();
						rowCount1++;
					} else {
						Statement mdbstmt = mdbcon.createStatement();
						ResultSet koders = mdbstmt.executeQuery("select MAX(KODE_PGL) from Data_Barang");
						while(koders.next()) {
							mdbin.setInt(1,koders.getInt(1)+1);
							mdbin.setString(2,dbfrs1.getString(1));
							mdbin.setDouble(3,dbfrs1.getDouble(2));
							mdbin.setDouble(4,dbfrs1.getDouble(3));
							mdbin.executeUpdate();
							rowCount2++;
						}
					}
				}
				dbfrs1.close();
				mdbup.close();
				dbfcon.close();
				mdbcon.close();
				System.out.println("	" + rowCount1 + " data yang di update");
				System.out.println("	" + rowCount2 + " data baru yang masuk");
				System.out.println("	Data Sayuran sudah diupdate ");
				System.out.println("");
			}

			catch(Exception e)
			{
				e.printStackTrace();
				System.out.println(e.getMessage());
				System.out.println("	Kode Panggil atau Kode PLU sudah ada. Data gagal di masukan!");
			}
		}


		void update5() {
			try	{
				Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
				Connection dbfcon = DriverManager.getConnection("jdbc:odbc:inv");
				Connection mdbcon = DriverManager.getConnection("jdbc:odbc:ygscale");
				Statement dbfstmt1 = dbfcon.createStatement();
				ResultSet dbfrs1 = dbfstmt1.executeQuery("select article,jual,plu from inv099 where rec > 0");
				PreparedStatement mdbps = mdbcon.prepareStatement("select * from Data_Barang where kode_plu = ?");
				PreparedStatement mdbup = mdbcon.prepareStatement("update Data_Barang set harga=? where kode_plu=?");
				PreparedStatement mdbin = mdbcon.prepareStatement("insert into Data_Barang(kode_pgl,nama,harga,kode_plu) values (?,?,?,?)");
				int rowCount1=0,rowCount2=0;
				while(dbfrs1.next()) {
					mdbps.setDouble(1,dbfrs1.getDouble(3));
					ResultSet mdbrs = mdbps.executeQuery();
					if(mdbrs.next()) {
						mdbup.setDouble(1,dbfrs1.getDouble(2));
						mdbup.setDouble(2,dbfrs1.getDouble(3));
						mdbup.executeUpdate();
						rowCount1++;
					} else {
						Statement mdbstmt = mdbcon.createStatement();
						ResultSet koders = mdbstmt.executeQuery("select MAX(KODE_PGL) from Data_Barang");
						while(koders.next()) {
							mdbin.setInt(1,koders.getInt(1)+1);
							mdbin.setString(2,dbfrs1.getString(1));
							mdbin.setDouble(3,dbfrs1.getDouble(2));
							mdbin.setDouble(4,dbfrs1.getDouble(3));
							mdbin.executeUpdate();
							rowCount2++;
						}
					}
				}
				dbfrs1.close();
				mdbup.close();
				dbfcon.close();
				mdbcon.close();
				System.out.println("	" + rowCount1 + " data yang di update");
				System.out.println("	" + rowCount2 + " data baru yang masuk");
				System.out.println("	Data Buah-buahan sudah diupdate ");
				System.out.println("");
			}

			catch(Exception e)
			{
				e.printStackTrace();
				System.out.println(e.getMessage());
				System.out.println("	Kode Panggil atau Kode PLU sudah ada. Data gagal di masukan!");
			}
		}



		void TableToDat() {
			try {
			Class.forName ("sun.jdbc.odbc.JdbcOdbcDriver");
			Connection conn = DriverManager.getConnection ("jdbc:odbc:ygscale");
			Statement stmt1 = conn.createStatement();
			ResultSet rs1 = stmt1.executeQuery("select NOMOR_MESIN from IP_Mesin");
			while (rs1.next()) {
				int nomor = rs1.getInt("NOMOR_MESIN");
				Statement stmt  = conn.createStatement();
				stmt.execute ("SELECT kode_pgl,status,harga,L1_fmt,L2_fmt,flag,"
				+"kode_plu,qty,simbol,tgl,bns,place,nama,pesan,bcc FROM Data_Barang");
				ResultSet rs = stmt.getResultSet();
				filename = "SM00"+nomor+"F25.dat";
				FileWriter writer = new FileWriter (filename);
				DecimalFormat fmt1 = new DecimalFormat("00000000");
				DecimalFormat fmt2 = new DecimalFormat("000000000000");
				DecimalFormat fmt3 = new DecimalFormat("00");
				DecimalFormat fmt4 = new DecimalFormat("00000000000000");
				while (rs.next()) {
					String kodepgl = fmt1.format(rs.getInt(1));
					writer.write(kodepgl);
					String nol1 = "00";
					writer.write(nol1);
					String sts = rs.getString(2);
					String stat;
					String nol2;
					if(sts.compareTo("kg")==0) {
						stat = "10004DA804";
						nol2 = "00000000000000000000000000000000000000";
					}
					else {
						stat = "1100CDA904";
						nol2 = "00000000000000000000000000000000000000000000";
					}
					String hrg = fmt1.format(rs.getInt(3));
					String lbl1 = rs.getString(4);
					String lbl2 = rs.getString(5);
					String flg = rs.getString(6);
					String plu = rs.getString(7);
					String plu2 = "0000000";
					String tanggal = fmt3.format(rs.getInt(10));
					String nol3 = "00000000000";
					String bonus = rs.getString(11);
					String place = fmt4.format(rs.getInt(12));
					String nama = rs.getString(13);
					String nol4;
					int charsize = (trimRight(nama).length());
					if (charsize<16) {
						nol4 = "0";
					}
					else {
						nol4 = "";
					}
					String chsize = Integer.toHexString(charsize);
					String ing = "0D0601200C0301200D0301200D0301200D0301200D0301200D0301200D0301200D030120";
					String pesan = rs.getString(14);
					String bcc = rs.getString(15);
					int rec_size = (kodepgl.length()+nol1.length()+sts.length()+stat.length()
					+nol2.length()+hrg.length()+lbl1.length()+lbl2.length()+flg.length()
					+plu.length()+plu2.length()+tanggal.length()+nol3.length()+bonus.length()
					+place.length()+(charsize*2)+nol4.length()+chsize.length()+ing.length()+pesan.length()
					+bcc.length())/2;
					String size = Integer.toHexString(rec_size);
					writer.write(size);
					writer.write(stat);
					writer.write(hrg);
					writer.write(lbl1);
					writer.write(lbl2);
					writer.write(flg);
					writer.write(plu);
					writer.write(plu2);
					writer.write(nol2);
					writer.write(tanggal);
					writer.write(nol3);
					writer.write(bonus);
					writer.write(place);
					writer.write(nol4);
					writer.write(chsize);
					for(int i = 0;i<trimRight(nama).length();i++) {
						int ch=(int)nama.charAt(i);
						String s4=Integer.toHexString(ch);
						writer.write(s4);
					}
					writer.write(ing);
					writer.write(pesan);
					writer.write(bcc);

				}
				rs.close();
				stmt.close();
				writer.close();;
				}
				System.out.println("	Table telah di konversi ke file DAT");

			}

			catch (Exception ex) {
				ex.printStackTrace();
			}
		}

		void hostfile() {
			try {
				Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
				Connection con = DriverManager.getConnection("jdbc:odbc:ygscale");
				Statement stmt = con.createStatement();
				ResultSet rs = stmt.executeQuery("select IP1,IP2,IP3,IP4,Name from IP_Mesin");
				ResultSetMetaData meta = rs.getMetaData();
				hostname = "C:\\WINDOWS\\system32\\drivers\\etc\\HOSTS";
				FileWriter writer = new FileWriter (hostname);
				int numCols = meta.getColumnCount();
				while (rs.next()) {
					for (int col=1; col<=numCols; col++) {
						String next = rs.getString(col);
						try {
							writer.write (next);
						} catch (NullPointerException ex) {}
						if (col<4) {
							writer.write (columnSeparator1);
						}
						else {
							writer.write (columnSeparator2);
						}
					}
				writer.write (rowSeparator);
				}
				writer.close();
				rs.close();
				stmt.close();
				con.close();
			}

			catch(Exception e)
			{
				System.out.println(e);
			}
		}


		void transfer() {
			try {
				Runtime r = Runtime.getRuntime();
				r.exec("E:\\ygscale\\twswtcp F25.DAT 1");
 				System.out.println("	Data telah dikirim");

			}

			catch(IOException e)
			{
				System.out.println("	Data tidak terkirim !");

			}


		}
	}
}

