import csv, pyodbc

# set up some constants
# tested on windows
MDB = 'DigiMap.mdb'
DRV = '{Microsoft Access Driver (*.mdb)}'
PWD = 'pw'

# connect to db
con = pyodbc.connect('DRIVER={};DBQ={};PWD={}'.format(DRV,MDB,PWD))
cur = con.cursor()

# run a query and get the results 
SQL = 'SELECT * FROM filestruct;' # your query goes here
rows = cur.execute(SQL).fetchall()
print(rows)
cur.close()
con.close()