from zk import ZK, const

conn = None
zk = ZK('192.168.168.64', port=4370, timeout=5)

try:
    print('Connecting to device ...')
    conn = zk.connect()
    users = conn.get_users()
    dict_user = {user.user_id : user.name for user in users}
    attendances = conn.get_attendance()
    for number, attendance in zip(range(len(attendances)),attendances):
        print(f"{number+1}. {attendance}")
except Exception as e:
    print(f"Error : {e}")
# finally:
    