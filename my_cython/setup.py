from setuptools import setup, Extension
from Cython.Build import cythonize

setup(
    ext_modules = cythonize(Extension(name="hello01", sources=["hello01.pyx"]))
)