from distutils.core import Extension, setup
from Cython.Build import cythonize

# define an extension that will be cythonized and compiled
ext = Extension(name="bello02", sources=["bello02.pyx"])
setup(ext_modules=cythonize(ext))