import pandas_access as mdb

db_filename = 'DigiMap.mdb'

# Listing the tables.
for tbl in mdb.list_tables(db_filename):
  print(tbl)

# Read a small table.
df = mdb.read_table(db_filename, "FILESTRUCTDATA")