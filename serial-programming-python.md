# Serial Programming Python

[Reading serial data in realtime in Python](https://stackoverflow.com/questions/19908167/reading-serial-data-in-realtime-in-python)

```python
While True:
    bytesToRead = ser.inWaiting()
    ser.read(bytesToRead)
```

You can use inWaiting() to get the amount of bytes available at the input queue.
Then you can use read() to read the bytes, something like that:

Why not to use readline() at this case from Docs:
Read a line which is terminated with end-of-line (eol) character (\n by default) or until timeout.
<br/>
<br/>


[python - How to read data fully from serial port?](https://stackoverflow.com/questions/51175606/python-how-to-read-data-fully-from-serial-port)

```python
buffer = bytes()  # .read() returns bytes right?
while True:
    if serial_port.in_waiting > 0:
        buffer += serial_port.read(serial_port.in_waiting)
        try:
            complete = buffer[:buffer.index(b'}')+1]  # get up to '}'
            buffer = buffer[buffer.index(b'}')+1:]  # leave the rest in buffer
        except ValueError:
            continue  # Go back and keep reading
        print('buffer=', complete)
        ascii = buffer.decode('ascii')
        print('ascii=', ascii)

```

NOTE1: I presume serial_port.in_waiting could in theory change between the if and read, but I also presume unread bytes just stay on buffer and we're fine.
NOTE2: This approach is a bit naive and does not take into account that you could have also read two chunks of JSON code.
NOTE3: And it also does not account for nested mappings in your JSON if that is the case.
Hopefully it's still helpful. Bottom line. Unless handling fixed size inputs or getting any other way to have pySerial feed you content chunked as desired, you have to read stuff in and process it in your script.
<br/>
<br/>

[Python Serial: How to use the read or readline function to read more than 1 character at a time](https://stackoverflow.com/questions/16077912/python-serial-how-to-use-the-read-or-readline-function-to-read-more-than-1-char)



I see a couple of issues.

### First:
    ser.read() 
is only going to return 1 byte at a time.

If you specify a count
    ser.read(5)
it will read 5 bytes (less if timeout occurrs before 5 bytes arrive.)
If you know that your input is always properly terminated with EOL characters, better way is to use
    ser.readline()

That will continue to read characters until an EOL is received.

### Second:

Even if you get ser.read() or ser.readline() to return multiple bytes, since you are iterating over the return value, you will still be handling it one byte at a time.
Get rid of the
    for line in ser.read():

and just say:
    line = ser.readline()

<br/>

## Speed up Pyserial

[How can I improve PySerial read speed](https://stackoverflow.com/questions/29557353/how-can-i-improve-pyserial-read-speed) : <br/>

1. I've switched from PySerial to PyTTY, which solves my problem.<br/> __Comment : I cant using this library__
1. The readline() method is telling pyserial to parse to the next line. You are also doing a decode() in the middle of your receive cycle. All of this is occurring right in the middle of your stream.<br/> __Comment: Yes. Dont use readline()__
1. class ReadLine: <br/>__comment : i already use this and not work__
<br/>

[How to quickly read single byte serial data using Python](https://stackoverflow.com/questions/26008937/how-to-quickly-read-single-byte-serial-data-using-python)
<br/>
[Faster method for clearing serial port on pyserial](https://stackoverflow.com/questions/66446813/faster-method-for-clearing-serial-port-on-pyserial)<br/>
[Pyserial buffer fills faster than I can read](https://stackoverflow.com/questions/10125009/pyserial-buffer-fills-faster-than-i-can-read)
__comment : many advice in here__

# PySerial
[Short introduction](https://pyserial.readthedocs.io/en/latest/shortintro.html)