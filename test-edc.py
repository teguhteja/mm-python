import serial
import traceback
#Can be Downloaded from this Link
#https://pypi.python.org/pypi/pyserial

#Global Variables
ser = 0

#Function to Initialize the Serial Port
def init_serial():
    ser = serial.Serial()
    ser.baudrate = 9600
    ser.port = 'COM3'  #COM Port Name Start from 0
    ser.parity = serial.PARITY_NONE
    ser.stopbits = serial.STOPBITS_ONE
    ser.bytesize = serial.EIGHTBITS

    #ser.port = '/dev/ttyUSB0' #If Using Linux

    #Specify the TimeOut in seconds, so that SerialPort
    #Doesn't hangs
    ser.timeout = 10
    
    ser.open()          #Opens SerialPort

    # print port open or closed
    if ser.isOpen():
        print('Open: ' + ser.portstr)
    return ser
#Function Ends Here

#Call the Serial Initilization Function, Main Program Starts from here
ser = init_serial()

try :
    temp = input('Type what you want to send, hit enter:\r\n')
    # ser.write(temp)
    req = bytes(temp, 'utf-8')
    ser.write(req)         #Writes to the SerialPort
   
    bytes = ser.readline()  #Read from Serial Port
    print(bytes) 
        
except :
    traceback.print_exc()